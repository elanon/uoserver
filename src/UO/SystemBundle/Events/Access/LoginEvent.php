<?php
namespace UO\SystemBundle\Events\Access;
use UO\EntityBundle\Document\Account;
use UO\ServerBundle\Logging\Logger;
use UO\ServerBundle\Tools\DataConverter;
use UO\ServerBundle\Tools\Packet;
use UO\ServerBundle\Tools\PacketReader;
use UO\SystemBundle\Events\MainEvent;
use UO\SystemBundle\Security\AccountFinder;


/**
 * Created by PhpStorm.
 * User: lan
 * Date: 17.04.17
 * Time: 19:14
 */
class LoginEvent
{
    private $serverInfo;

    private $accountFinder;

    /**
     * @var MainEvent $event
     */
    private $event;

    public function __construct(AccountFinder $accountFinder, array $serverInfo)
    {
        $this->serverInfo = $serverInfo;
        $this->accountFinder = $accountFinder;
    }

    public function onLogin(MainEvent $event)
    {
        $this->event = $event;
        $packetReader = new PacketReader($this->event->getMessage());
        $login = $packetReader->getByteFrom(2, 30);
        $password = $packetReader->getByteFrom(32, 30); //todo some byte on the end?


        if($this->accountFinder->checkAccountAccess($login, $password)) {
            Logger::Log('try to log:'.$login . '/' . $password.';');
            $account = $this->accountFinder->getAccount($login, $password);
            $this->accountFinder->updateLoginInformation($account, $this->event->getClientResource());
            return $this->loginSuccess($account);
        }

        if($this->serverInfo['auto_account'] && $this->accountFinder->checkAccountExist($login) === false) {
                Logger::Log('account not found, but autoaccounting is on. Account created;');
                $account = $this->accountFinder->createUser($login, $password, $event->getClientResource());
                return $this->loginSuccess($account);
        }

        Logger::Log('login for credientals: '.$login.'/'.$password.' failure;');
        $this->loginFailure($login);
    }

    private function loginSuccess(Account $account)
    {
        $ip = explode(".", $this->serverInfo['host']);
        $packet = new Packet;
        $packet
            ->prependPacket("A8")
            ->appendPacket("FF")
            ->appendPacket(DataConverter::convertDecimalToHex(1), 4)
            ->appendPacket(DataConverter::convertDecimalToHex(1), 4)
            ->appendPacket(DataConverter::convertStringToHex($this->serverInfo['name']), 64, Packet::PAD_FROM_RIGHT)
            ->appendPacket(DataConverter::convertDecimalToHex($this->serverInfo['full']), 2) //todo full is not from PARAM!!!
            ->appendPacket(DataConverter::convertDecimalToHex($this->serverInfo['timezone']), 2);
        for ($i=3; $i>=0; $i--) {
            $packet->appendPacket(DataConverter::convertDecimalToHex($ip[$i]), 2);
        }

        $currentLenght = $packet->getPacketSize();
        $packet->injectPacket(DataConverter::convertDecimalToHex($currentLenght + 2), 4, 2);
        $this->event->getClient()->send($packet->getPacket());
        return true;
    }

    /**
     * TODO FAILURE FROM BANS ETC.
     * @return bool
     */
    private function loginFailure($login)
    {
        $reason = 0;

        if ($this->accountFinder->checkAccountExist($login)) {
            $reason = 3;
        }

        $packet = new Packet;
        $packet
            ->prependPacket("82")
            ->appendPacket(DataConverter::convertDecimalToHex($reason), 2);
        $this->event->getClient()->send($packet->getPacket());
        return true;
    }
}