<?php
namespace UO\SystemBundle\Events\Access;
use UO\EntityBundle\Document\Account;
use UO\ServerBundle\Logging\Logger;
use UO\ServerBundle\Tools\DataConverter;
use UO\ServerBundle\Tools\Packet;
use UO\ServerBundle\Tools\PacketReader;
use UO\SystemBundle\Events\MainEvent;
use UO\SystemBundle\Security\AccountFinder;


/**
 * Created by PhpStorm.
 * User: lan
 * Date: 17.04.17
 * Time: 19:14
 */
class ServerPickEvent
{
    private $accountFinder;

    /**
     * @var MainEvent $event
     */
    private $event;

    public function __construct(AccountFinder $accountFinder)
    {
        $this->accountFinder = $accountFinder;
    }

    public function onServerSelect(MainEvent $event)
    {
        $this->event = $event;
        $packetReader = new PacketReader($this->event->getMessage());
        $server = ord($packetReader->getByteFrom(2, 2));
        Logger::Log('client pick server number: ' . $server);
        //todo check server select


        //todo modify for newer clients
        $packet = new Packet();
        $packet
            ->prependPacket("A9")
            ->appendPacket(DataConverter::convertDecimalToHex(0), 2)
            ->appendPacket(DataConverter::convertDecimalToHex(0), 2)
            ->appendPacket(DataConverter::convertDecimalToHex(0), 2)
            ->appendPacket(DataConverter::convertDecimalToHex(1), 2)
            ->appendPacket(DataConverter::convertDecimalToHex(0), 2)
            ->appendPacket(DataConverter::convertStringToHex('Minoc'), 64)
            ->appendPacket(DataConverter::convertStringToHex('Felucca'), 64)
            ->appendPacket(DataConverter::convertDecimalToHex(4), 8);

        $currentLenght = $packet->getPacketSize();
        $packet->injectPacket(DataConverter::convertDecimalToHex($currentLenght + 2), 4, 2);


        $this->event->getClient()->send($packet->getPacket());

        return true;
    }

    public function getCharacterList()
    {

    }
}