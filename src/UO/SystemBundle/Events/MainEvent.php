<?php
namespace UO\SystemBundle\Events;

use Ratchet\ConnectionInterface;
use Symfony\Component\EventDispatcher\Event;

/**
 * Created by PhpStorm.
 * User: lan
 * Date: 14.04.17
 * Time: 18:42
 */
class MainEvent extends Event
{
    protected $client;

    protected $message;



    public function __construct(ConnectionInterface $client, $message)
    {
        $this->client = $client;
        $this->message = $message;
    }

    /**
     * @return ConnectionInterface
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @return mixed
     */
    public function getClientResource()
    {
        return $this->client->resourceId;
    }

    /**
     * @return mixed
     */
    public function getClientAdress()
    {
        return $this->client->remoteAddress;
    }
}