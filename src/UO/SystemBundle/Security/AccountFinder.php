<?php

namespace UO\SystemBundle\Security;
use Doctrine\ODM\MongoDB\DocumentManager;
use UO\EntityBundle\Document\Account;


/**
 * Created by PhpStorm.
 * User: lan
 * Date: 04.07.17
 * Time: 19:56
 */
class AccountFinder
{

    private $odm;

    public function __construct(DocumentManager $odm)
    {
        $this->odm = $odm;
    }

    public function checkAccountExist($login)
    {
        $account = $this->odm->getRepository('UOEntityBundle:Account')->findOneBy(array(
            'login' => $login
        ));

        if ($account instanceof Account) {
            return true;
        } else {
            return false;
        }
    }

    // todo some security for multi accounting
    public function checkAccountAccess($login, $password)
    {

        $account = $this->odm->getRepository('UOEntityBundle:Account')->findOneBy(array(
            'login' => $login,
            'password' => sha1($password)
        ));

        if ($account instanceof Account) {
            return true;
        } else {
            return false;
        }
    }

    public function getAccount($login,$password)
    {
        $account = $this->odm->getRepository('UOEntityBundle:Account')->findOneBy(array(
            'login' => $login,
            'password' => sha1($password)
        ));

        return $account;
    }

    public function updateLoginInformation(Account $account, $clientId)
    {
        $account->setClientId($clientId);
        $account->setLastLogin(new \DateTime());
        $this->odm->flush($account);
    }

    public function createUser($login, $password, $clientId)
    {
        $account = new Account();
        $account->setLogin($login);
        $account->setPassword(sha1($password));
        $account->setCreatedAt(new \DateTime());
        $account->setLastLogin(new \DateTime());
        $account->setClientId($clientId);
        $this->odm->persist($account);
        $this->odm->flush($account);
        return $account;
    }
}