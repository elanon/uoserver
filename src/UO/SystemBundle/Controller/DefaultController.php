<?php

namespace UO\SystemBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('UOSystemBundle:Default:index.html.twig');
    }
}
