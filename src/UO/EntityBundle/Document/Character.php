<?php
namespace UO\EntityBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as Mongo;

/**
 * Class Character
 * @package UO\EntityBundle\Document
 * @Mongo\Document(repositoryClass="UO\EntityBundle\Repository\CharacterRepository")
 */
class Character
{

    /**
     * @Mongo\Id(strategy="AUTO")
     */
    private $id;

    /**
     * @Mongo\Field(type="string")
     */
    private $name;

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }
}
