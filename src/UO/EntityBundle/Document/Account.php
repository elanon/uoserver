<?php
namespace UO\EntityBundle\Document;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as Mongo;

/**
 * Class Character
 * @package UO\EntityBundle\Document
 * @Mongo\Document(repositoryClass="UO\EntityBundle\Repository\AccountRepository")
 */
class Account
{

    /**
     * @Mongo\Id(strategy="AUTO")
     */
    private $id;

    /**
     * @Mongo\Field(type="string")
     */
    private $clientId;

    /**
     * @Mongo\Field(type="string")
     */
    private $login;

    /**
     * @Mongo\Field(type="string")
     */
    private $password;

    /**
     * @Mongo\Field(type="date")
     */
    private $lastLogin;

    /**
     * @Mongo\Field(type="date")
     */
    private $createdAt;

    /**
     * @Mongo\ReferenceMany(targetDocument="Character", cascade="{'persist', 'remove'}")
     */
    private $characters;



    public function __construct()
    {
        $this->characters = new ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set clientId
     *
     * @param string $clientId
     * @return $this
     */
    public function setClientId($clientId)
    {
        $this->clientId = $clientId;
        return $this;
    }

    /**
     * Get clientId
     *
     * @return string $clientId
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * Set login
     *
     * @param string $login
     * @return $this
     */
    public function setLogin($login)
    {
        $this->login = $login;
        return $this;
    }

    /**
     * Get login
     *
     * @return string $login
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * Set lastLogin
     *
     * @param $lastLogin
     * @return $this
     */
    public function setLastLogin($lastLogin)
    {
        $this->lastLogin = $lastLogin;
        return $this;
    }

    /**
     * Get lastLogin
     *
     * @return $lastLogin
     */
    public function getLastLogin()
    {
        return $this->lastLogin;
    }

    /**
     * Set createdAt
     *
     * @return $this
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return $createdAt
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Add character
     *
     * @param Character $character
     */
    public function addCharacter(Character $character)
    {
        $this->characters[] = $character;
    }

    /**
     * Remove character
     *
     * @param Character $character
     */
    public function removeCharacter(Character $character)
    {
        $this->characters->removeElement($character);
    }

    /**
     * Get characters
     *
     * @return \Doctrine\Common\Collections\Collection $characters
     */
    public function getCharacters()
    {
        return $this->characters;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return $this
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * Get password
     *
     * @return string $password
     */
    public function getPassword()
    {
        return $this->password;
    }
}
