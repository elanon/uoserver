<?php

namespace UO\ServerBundle\Command;

use Ratchet\Server\IoServer;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class runServerCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:run')
            ->setDescription('Command to run server');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Server starting');
        $communication = $this->getContainer()->get('uo_server.server_communication');

        $server = IoServer::factory(
                    $communication,
                2593,
                '127.0.0.1');

        $server->run();
    }
}


