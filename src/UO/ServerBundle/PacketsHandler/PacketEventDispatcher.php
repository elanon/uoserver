<?php
namespace UO\ServerBundle\PacketsHandler;

use Ratchet\ConnectionInterface;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use UO\ServerBundle\Logging\Logger;
use UO\ServerBundle\Tools\DataConverter;
use UO\SystemBundle\Events\MainEvent;

/**
 * Created by PhpStorm.
 * User: lan
 * Date: 13.04.17
 * Time: 00:27
 */
class PacketEventDispatcher
{

    private $packetsEvents;
    private $dispatcher;


    public function __construct($packetsEvents, EventDispatcherInterface $dispatcher)
    {
        $this->packetsEvents = $packetsEvents['packets'];
        $this->dispatcher = $dispatcher;
    }

    public function dispatchEvent(ConnectionInterface $sender, $msg)
    {
        $data = unpack('C*',$msg);
        $eventPacket = "0x".DataConverter::convertDecimalToHex($data[1]);
        $event = isset($this->packetsEvents[$eventPacket]) ? $this->packetsEvents[$eventPacket] : null;

        dump($eventPacket);
        if ($event !== null) {
            $incomingEvent = new MainEvent($sender, $msg);
            dump($event);
            dump($msg);
            if ($this->dispatcher->hasListeners($event)) {
                $this->dispatcher->dispatch($event, $incomingEvent);
            } else {
                Logger::Log('Event :'.$event.' is not implement yet!');
            }

        }
    }
}