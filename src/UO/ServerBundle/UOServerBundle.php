<?php

namespace UO\ServerBundle;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use UO\ServerBundle\Extension\EventDispatcherCompilerPass;

class UOServerBundle extends Bundle
{
    public function build(ContainerBuilder $containerBuilder)
    {
        parent::build($containerBuilder);

//        $containerBuilder->addCompilerPass(new EventDispatcherCompilerPass());
    }
}
