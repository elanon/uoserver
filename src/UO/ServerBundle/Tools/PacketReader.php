<?php
namespace UO\ServerBundle\Tools;



/**
 * Created by PhpStorm.
 * User: lan
 * Date: 17.04.17
 * Time: 21:19
 */
class PacketReader
{
    private $packetData;

    public function __construct($packet)
    {
        $this->packetData = unpack('C*', $packet);
    }

    public function getByte($number)
    {
        if (isset($this->packetData[$number])) {
            return chr($this->packetData[$number]);
        } else {
            return false;
        }
    }

    public function getByteFrom($from, $lenght, $ignoreNull = true)
    {
        $string = '';

        for($i = 0; $i < $lenght; $i++) {
            $string .= $this->getByte($from);
            $from++;
        }

        if ($ignoreNull === true) {
           return trim($string);
        } else {
            return $string;
        }

    }
}