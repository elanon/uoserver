<?php
namespace UO\ServerBundle\Tools;



/**
 * Created by PhpStorm.
 * User: lan
 * Date: 17.04.17
 * Time: 21:19
 */
class Packet
{

    CONST PAD_FROM_LEFT = 0;
    CONST PAD_FROM_RIGHT =  1;

    private $message;

    public function appendPacket($data, $size = null, $padding = self::PAD_FROM_LEFT, $complain = "0")
    {
        $size = $size === null ? strlen($data) : $size;
        $this->message = $this->message.str_pad($data, $size, $complain, $padding);
        return $this;
    }

    public function prependPacket($data, $size = null, $padding = self::PAD_FROM_LEFT, $complain = "0")
    {
        $size = $size === null ? strlen($data) : $size;
        $this->message = str_pad($data, $size, $complain, $padding).$this->message;
        return $this;
    }

    /**
     * @param $data
     * @param $size int size of byte
     * @param $position integer number of byte
     * @param int $padding
     * @param string $complain
     * @return Packet
     */
    public function injectPacket($data, $size = null, $position,$padding = self::PAD_FROM_LEFT, $complain = "0")
    {
        $start = $position*2-2;
        $size = $size === null ? strlen($data) : $size;
        $this->message = substr_replace($this->message, str_pad($data, $size, $complain, $padding), $start,0);
        return $this;
    }

    public function getPacketSize()
    {
        return ceil(strlen($this->message) / 2);
    }

    public function getPacket()
    {
        return pack('H*', $this->message);
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function __toString()
    {
        return pack('H*', $this->message);
    }
}