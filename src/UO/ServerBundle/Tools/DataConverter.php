<?php
/**
 * Created by PhpStorm.
 * User: lan
 * Date: 18.04.17
 * Time: 21:44
 */

namespace UO\ServerBundle\Tools;


class DataConverter
{
    static function convertStringToHex($string)
    {
        $hex = null;
        for ($i=0; $i<strlen($string); $i++) {
            $hex .= substr('0'.dechex(ord($string[$i])), -2);
        }

        return strtoupper($hex);
    }

    static function convertDecimalToHex($string)
    {
        return strtoupper(dechex($string));
    }

    static function convertHexToDecimal($string)
    {
        return hexdec($string);
    }
}