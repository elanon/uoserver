<?php

namespace UO\ServerBundle\Controller;

use Cocur\Slugify\Slugify;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function packetEventsCreatorAction()
    {
        $slugger = new Slugify();
        $dom = new \DOMDocument();
        libxml_use_internal_errors(true);
        $dom->loadHTMLFile('http://necrotoolz.sourceforge.net/kairpacketguide/');
        $data = $dom->getElementsByTagName('table');
        $table = $data->item(0);
        $rows = $table->getElementsByTagName("tr");

        /**
         * @var \DOMElement $row
         */
        foreach($rows as $row) {
            $cells = $row->getElementsByTagName('td');
            $packet = $cells->item(0);
            $packetName = $cells->item(1);
            $lenght = $cells->item(2);
            $clientUse = $cells->item(3);
            $serverUse = $cells->item(4);
            $obsolote = $cells->item(5);

            if($packet instanceof \DOMElement && $packetName instanceof \DOMElement) {
                $yamlLine = $packet->nodeValue;
                $yamlLine .= ': event.'.$slugger->slugify($packetName->nodeValue,'.');
                $yamlLine .= ' #';
                if ($lenght instanceof \DOMElement) {
                    if ($lenght->nodeValue == "dynamic") {
                        $yamlLine .= " Lenght: Dynamic";
                    } elseif(hexdec($lenght->nodeValue)>0) {
                        $yamlLine .= " Lenght: ".hexdec($lenght->nodeValue);
                    }
                }
                    $yamlLine .= "<br>";
                echo $yamlLine;
            }
        }

        die;
        return $this->render('UOServerBundle:Default:index.html.twig');
    }
}
